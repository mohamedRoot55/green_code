import 'package:flutter/material.dart';
import 'package:greencode_task/MainTabs/widgets/nav_item_model.dart';

import 'nav_item.dart';

class NavBar extends StatelessWidget {
  final List<NavBottomItemModel> items;
  final Function(int index) setSelectedIndex;
  final int selectedIndex;

  NavBar(
    this.items,
    this.selectedIndex,
    this.setSelectedIndex,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .09,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: items.map((item) {
          var index = items.indexOf(item);
          return Expanded(
            flex: 1,
            child: GestureDetector(
              onTapDown: (_) => this.setSelectedIndex(index),
              child: NavItem(item, this.selectedIndex == index),
            ),
          );
        }).toList(),
      ),
    );
  }
}
