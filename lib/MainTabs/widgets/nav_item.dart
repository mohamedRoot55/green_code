import 'package:flutter/material.dart';
import 'package:greencode_task/MainTabs/widgets/nav_item_model.dart';
import 'package:greencode_task/constants/colors.dart';


class NavItem extends StatelessWidget {
  final NavBottomItemModel item;
  final bool selected;

  NavItem(this.item, this.selected);

  @override
  Widget build(BuildContext context) {

    final height = MediaQuery.of(context).size.height ;
    return Container(
      height: double.maxFinite,
      //      decoration: BoxDecoration(
      //        color: grey
      //      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
                color: this.selected ? purple : Colors.white,
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: <Widget>[

                Image.asset(item.image , width: height * .02, color: this.selected ? Colors.white : Colors.black , ) ,
//                Icon(
//                  this.item.icon,
//                  size: 16,
//                  color: this.selected ? Colors.white : Colors.black,
//                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: DefaultTextStyle.merge(
                    style: TextStyle(
                        fontFamily: 'Arb',
                        color: this.selected ? Colors.white : Colors.black,
                        fontSize: 11,
                        fontWeight: FontWeight.w400),
                    child: Text(this.item.label),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
