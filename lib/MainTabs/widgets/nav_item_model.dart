import 'package:flutter/material.dart';

class NavBottomItemModel {
  NavBottomItemModel(this.icon, this.label , this.image);

  IconData icon;
  setIcon(IconData value) => icon = value;

  String image ;
  setImage(String image) {
    this.image = image ;
  }




  String label;
  setLabel(String value) => label = value;
}
