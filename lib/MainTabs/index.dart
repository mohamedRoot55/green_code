 import 'package:flutter/material.dart';
import 'package:greencode_task/MainTabs/widgets/nav_bar.dart';
import 'package:greencode_task/providers/main_tabs_provider.dart';
import 'package:provider/provider.dart';


class MainTaps extends StatefulWidget {
  static final String routeName = '/MainTaps';

  @override
  _MainTapsState createState() => _MainTapsState();
}

class _MainTapsState extends State<MainTaps>  {



  @override
  Widget build(BuildContext context) {
    return Consumer<MainTapsProvider>(
      builder: (ctx, client, ch) {
        client.setContext(context);
        return Scaffold(
          body: client.selectedPage,
          bottomNavigationBar: NavBar(
              client.items, client.selectedIndex, client.setSelectedIndex),
        );
      },
    );
  }
}
