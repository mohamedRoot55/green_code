import 'package:after_layout/after_layout.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:greencode_task/AppModel.dart';
import 'package:greencode_task/providers/auth.dart';
import 'package:greencode_task/providers/main_tabs_provider.dart';
import 'package:greencode_task/routes/home.dart';
import 'package:greencode_task/routes/login.dart';
import 'package:greencode_task/routes/register.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'MainTabs/index.dart';

class MyApp extends StatefulWidget  {


  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with AfterLayoutMixin  {
  AppModel appModel  = new AppModel();
  bool isLogin = false;
  bool isChecking = true;


  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool login = (prefs.getBool('login') ?? false);

    return login ;
  }

  Widget renderFirstScreen() {
    if (isLogin)
      return MainTaps();
    else {
      return Login();
    }
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    await appModel.getConfig() ;
    setState(() {
      isLogin = appModel.isLogin ;
      isChecking = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isChecking) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: Container(),
        ),
      );
    }

    return ChangeNotifierProvider(
      create: (ctx) => appModel,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (ctx) => AuthProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => MainTapsProvider(),
          )
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: renderFirstScreen(),
          routes: {
            MainTaps.routeName : (ctx) => MainTaps() ,
            Login.routeName: (ctx) => Login(),
            HomePage.routeName: (ctx) => HomePage(),
            Register.routeName: (ctx) => Register(),
          },
        ),
      ),
    );
  }
}
