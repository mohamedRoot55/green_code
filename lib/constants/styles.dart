import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'colors.dart';

TextStyle logoStyle = TextStyle(
    color: purple ,
    fontWeight: FontWeight.bold ,
    fontSize: 60
) ;

TextStyle headerStyle = TextStyle(
    color: purple ,
    fontWeight: FontWeight.bold ,
    fontSize: 40
) ;


TextStyle commonWhiteStyle = TextStyle(
    color: Colors.white ,
    fontWeight: FontWeight.bold ,
    fontSize: 20
) ;

TextStyle commonPurpleStyle = TextStyle(
    color: purple ,
    fontWeight: FontWeight.bold ,
    fontSize: 16
) ;

InputDecoration getAuthInputDecoration(BuildContext context, String hintText) {
  return InputDecoration(

      fillColor: Colors.white,
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        borderSide: BorderSide(width: 1, color: darkGrey),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        borderSide: BorderSide(width: 1, color: Colors.orange),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        borderSide: BorderSide(width: 1, color: darkGrey),
      ),
      border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(
            width: 1,
          )),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(width: 1, color: darkGrey)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          borderSide: BorderSide(width: 1, color: darkGrey)),
      errorStyle: TextStyle(
        color: darkGrey,
      ),
      contentPadding: EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 10.0),
      hintStyle: TextStyle(
        color: darkGrey,
      ),
      hintText: hintText);
}
