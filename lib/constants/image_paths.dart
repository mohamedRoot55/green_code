const bgImage = "assets/bg.jpeg";
const cloud = "assets/cloud.png";
const facebook = "assets/facebook.png";
const google = "assets/google.png";
const shop = "assets/shop.png";
const notification = "assets/notification.png";
const product = "assets/product.png";
const discount = "assets/discount.png";
const microphone = "assets/microphone.png";
const cart = "assets/cart.png";
const more = "assets/more.png";