import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:greencode_task/MainTabs/index.dart';
import 'package:greencode_task/utils/helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  bool loading = false;

  setLoading(bool value) {
    loading = value;
    notifyListeners();
  }

  bool offline = false;

  setOffline(bool value) {
    offline = value;
    notifyListeners();
  }

  bool _isLoggedIn = false;
  Map userProfile;

  FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  void addUser(
      String fullName, String email, String gender, String phoneNumber) async {
    // Call the user's CollectionReference to add a new user
    await users
        .add({
          'full_name': fullName, // John Doe
          'email': email, // Stokes and Sons
          'gender': gender,
          'phoneNumber': phoneNumber,
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  void createUser(
      {String email,
        String password,
        String fullName,
        String gender,
        String phoneNumber,
        BuildContext context,
        snackBar,
        scaffoldKey}) async {
    setLoading(true);
    setOffline(false);
    try {


      SharedPreferences sharedPreferences = await SharedPreferences.getInstance() ;


      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((value) {
        print("createUser2");
        addUser(fullName, email, gender, phoneNumber);
        setLoading(false);
        sharedPreferences.setBool("login", true) ;
        Helper.navigateAndDeleteOldBackStacks(MainTaps.routeName, context);
      }).catchError((error) {
        setLoading(false);
        Helper.showSnackBar(snackBar, scaffoldKey, error.toString());
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Helper.showSnackBar(snackBar, scaffoldKey, "weak-password");
        setLoading(false);
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        setLoading(false);
        Helper.showSnackBar(snackBar, scaffoldKey, "email-already-in-use");
        print('The account already exists for that email.');
      }
    } catch (e) {
      setLoading(false);
      Helper.showSnackBar(snackBar, scaffoldKey, e.toString());
      print(e.toString());
    }
  }


  void login(
      {String email,
        String password,
        String fullName,
        String gender,
        String phoneNumber,

        BuildContext context,
        snackBar,
        scaffoldKey}) async {
    setLoading(true);
    setOffline(false);
    try {
     SharedPreferences sharedPreferences = await SharedPreferences.getInstance() ;

      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
        email: email,
        password: password,
      )
          .then((value) {
        print("createUser2");
        addUser(fullName, email, gender, phoneNumber);
        sharedPreferences.setBool("login", true) ;
        setLoading(false);

        Helper.navigateAndDeleteOldBackStacks(MainTaps.routeName, context);
      }).catchError((error) {
        setLoading(false);
        Helper.showSnackBar(snackBar, scaffoldKey, error.toString());
      });
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        Helper.showSnackBar(snackBar, scaffoldKey, "weak-password");
        setLoading(false);
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        setLoading(false);
        Helper.showSnackBar(snackBar, scaffoldKey, "email-already-in-use");
        print('The account already exists for that email.');
      }
    } catch (e) {
      setLoading(false);
      Helper.showSnackBar(snackBar, scaffoldKey, e.toString());
      print(e.toString());
    }
  }

  Future signInWithFacebook(snackBar, scaffoldKey, context) async {
    setLoading(true);
    try {

      SharedPreferences sharedPreferences = await SharedPreferences.getInstance() ;
      final LoginResult result =
          await FacebookAuth.instance.login(permissions: ["email"]);

      if (result.status == 200) {
        final FacebookAuthCredential facebookAuthCredential =
            FacebookAuthProvider.credential(result.accessToken.token);
        await FirebaseAuth.instance
            .signInWithCredential(facebookAuthCredential).then((value) {
          sharedPreferences.setBool("login", true) ;
          setLoading(false);
          Helper.navigateAndDeleteOldBackStacks(MainTaps.routeName, context);
        }).catchError((error){
          Helper.showSnackBar(snackBar, scaffoldKey, error.toString());
          setLoading(false);
         });

      }
    } on FirebaseAuthException catch (e) {
      Helper.showSnackBar(snackBar, scaffoldKey, e.code);
      setLoading(false);
      throw e;
    } catch (e) {
      setLoading(false) ;
      throw e;
    }
  }

  // todo >> this function also work but in case i need to store user data
  /// loginWithFB() async {
  ///    final result = await FacebookAuth.instance.login();
  ///    switch (result.status) {
  ///      case FacebookAuthLoginResponse.ok:
  ///        final token = result.accessToken.token;
  ///        final graphResponse = await http.get(
  ///            'https://graph.facebook.com/v2.12/me?fields=name,gender,phone_number,email&access_token=$token');
  ///        final profile = JSON.jsonDecode(graphResponse.body);
  ///        print(profile);
  ///        break;
  ///      case FacebookAuthLoginResponse.cancelled:
  ///        break;
  ///      default:
  ///        print("login failed");
  ///    }
  ///  }

  Future signInWithGoogle(snackBar, scaffoldKey, context) async {
    // Trigger the authentication flow
    setLoading(true) ;
    try {
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance() ;
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      // Once signed in, return the UserCredential
      await FirebaseAuth.instance.signInWithCredential(credential).then((value) {
        sharedPreferences.setBool("login", true) ;
        Helper.navigateAndDeleteOldBackStacks(MainTaps.routeName, context);
      })
          .catchError((error){
            setLoading(false);
        Helper.showSnackBar(snackBar, scaffoldKey, error.toString());
      });

    } on FirebaseAuthException catch (e) {
      print("FirebaseAuthException");
      Helper.showSnackBar(snackBar, scaffoldKey, e.code);
      setLoading(false);
      throw e;
    } catch (e) {
      setLoading(false) ;
      throw e;
    }
  }
}
