import 'package:flutter/cupertino.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:greencode_task/MainTabs/widgets/nav_item_model.dart';
import 'package:greencode_task/constants/image_paths.dart';
import 'package:greencode_task/routes/demo_page.dart';
import 'package:greencode_task/routes/home.dart';


class MainTapsProvider with ChangeNotifier {
  List<NavBottomItemModel> items = [];

  List<Widget> pages = [  HomePage(), DemoPage(), DemoPage(), DemoPage() , DemoPage()];

  setContext(BuildContext context) {
    items = [
      NavBottomItemModel(Feather.menu, "home" , shop),
      NavBottomItemModel(Feather.home, "discount", discount),
      NavBottomItemModel(Feather.calendar, "voice order" , microphone),
      NavBottomItemModel(Feather.user, "cart" , cart),
      NavBottomItemModel(Feather.user, "more" , cart),
    ];
  }

  int selectedIndex = 0;

  setSelectedIndex(int value) {
    selectedIndex = value;
    notifyListeners();
  }

  Widget get selectedPage {
    return pages[selectedIndex];
  }
}
