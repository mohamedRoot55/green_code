import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppModel with ChangeNotifier {
  bool isLogin = false;

  AppModel() {
    getConfig();
  }

  Future<bool> getConfig() async {
    try {
      isLogin = await checkLogin();
      notifyListeners();
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<bool> checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool("login") ?? false;
  }
}
