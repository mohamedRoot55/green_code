import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:greencode_task/constants/styles.dart';


class Helper {
  static void navigateAndDeleteOldBackStacks(
      String route, BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      Navigator.of(context)
          .pushNamedAndRemoveUntil(route, (Route<dynamic> route) => false);
    });
  }
  static void showSnackBar(
      SnackBar snackBar, GlobalKey<ScaffoldState> scaffoldKey, String message) {
    snackBar = SnackBar(
        content: Text(
          message,
          style: commonWhiteStyle,
        ));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }

  static String englishNumber(String number) {
    return number
        .replaceAll("١", "1")
        .replaceAll("٢", "2")
        .replaceAll("٣", "3")
        .replaceAll("٤", "4")
        .replaceAll("٥", "5")
        .replaceAll("٦", "6")
        .replaceAll("٧", "7")
        .replaceAll("٨", "8")
        .replaceAll("٩", "9")
        .replaceAll("٠", "0");
  }





}
