import 'package:flutter/material.dart';
import 'package:greencode_task/constants/colors.dart';
import 'package:greencode_task/constants/image_paths.dart';
class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size ;
    return Container(
      decoration: BoxDecoration(
          border:
          Border.all(color: Colors.grey, width: .5)),
      width: size.width * 0.35,
      height: size.height * .2,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            FittedBox(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius:
                    BorderRadius.circular(7)),
                padding: EdgeInsets.all(3),
                child: Text("12 %  discount"),
              ),
            ),
            Container(
                width: double.infinity,
                child: Center(
                    child: Image.asset(
                      product,
                      width: size.width * .1,
                    ))),
            Text("this is a demo "),
            Text("500 ml"),
            Text("22.61 SAR/bullet"),
            Row(
              children: <Widget>[
                Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Text(
                      "22.61",
                      style:
                      TextStyle(color: Colors.grey),
                    ),
                    Align(
                        alignment: Alignment.center,
                        child: Container(
                          color: Colors.grey,
                          height: 1,
                          width: size.width * .08,
                        ))
                  ],
                ),
                Text(
                  "SAR",
                  style: TextStyle(color: Colors.grey),
                ) ,



              ],
            ) ,
            SizedBox(
              height: size.height * .03,
            ) ,
            FittedBox(
              child: Container(
                width: size.width * .3,
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    color: purple ,
                    borderRadius: BorderRadius.circular(15)
                ),
                child: Center(
                  child: Text("add to cart" , style: TextStyle(
                      color: Colors.white , fontSize: 15
                  ),),
                ),
              ),
            )
          ],
        ),
      ),
    ) ;
  }
}
