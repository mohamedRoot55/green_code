import 'package:flutter/material.dart';
import 'package:greencode_task/constants/image_paths.dart';
class CategoryItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size ;
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(10),
            width: size.width * .2,
            height: size.height * .1,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15) ,
              border: Border.all(color: Colors.grey , width: 1)
            ),
            child: Center(child: Image.asset(product , width: size.width * .1,)),
          ) ,
          Text('category'
          )
        ],
      ),
    ) ;
  }
}
