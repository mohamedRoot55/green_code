import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:greencode_task/constants/colors.dart';
import 'package:greencode_task/constants/image_paths.dart';
import 'package:greencode_task/constants/styles.dart';
import 'package:greencode_task/widgets/category_item.dart';
import 'package:greencode_task/widgets/product_item.dart';

class HomePage extends StatefulWidget {
  static final String routeName = "/HomePage";

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final List<String> imgList = [
    'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
    'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
    'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
    'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
  ];
  CarouselController buttonCarouselController = CarouselController();
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.notifications,
          color: purple,
        ),
        actions: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: size.width * .02),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  "online shopping",
                  style: TextStyle(
                    color: purple,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  width: size.width * .02,
                ),
                Image.asset(
                  shop,
                  width: 30,
                ),
              ],
            ),
          )
        ],
      ),
      body: Container(
        height: size.height,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: CarouselSlider.builder(
                  options: CarouselOptions(
                      carouselController: buttonCarouselController,
                      autoPlay: true,
                      enlargeCenterPage: true,
                      viewportFraction: 0.9,
                      aspectRatio: 2.0,
                      initialPage: 2,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _current = index;
                        });
                      }),
                  itemCount: imgList.length,
                  itemBuilder: (ctx, index) {
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(24),
                      child: Image.network(imgList[index]),
                    );
                  },
//            items: imgList.map((e) => Image.network(e)).toList(),
//            carouselController: buttonCarouselController,
//            options: CarouselOptions(
//
//              autoPlay: true,
//              enlargeCenterPage: true,
//              viewportFraction: 0.9,
//              aspectRatio: 2.0,
//              initialPage: 2,
//            ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: imgList.map((url) {
                  int index = imgList.indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              ),
              Container(
                padding: EdgeInsets.all(4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border.all(color: Colors.grey)),
                width: size.width * .8,
                height: size.height * .05,
                child: Row(
                  children: <Widget>[
                    Icon(
                      Feather.search,
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: size.width * .03,
                    ),
                    Text(
                      "search",
                      style: TextStyle(color: Colors.grey),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: size.height * .03,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "special offers",
                      style: commonPurpleStyle,
                    ),
                    Text(
                      "more",
                      style: TextStyle(color: purple),
                    ),
                  ],
                ),
              ),
              Container(
                  height: size.height * .3,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 9,
                      itemBuilder: (ctx, index) => ProductItem())),
              SizedBox(
                height: size.height * .03,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "categories",
                      style: commonPurpleStyle,
                    ),
                    Text(
                      "more",
                      style: TextStyle(color: purple),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: size.height * .01,
              ),
              Container(
                  height: size.height * .3,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 9,
                      itemBuilder: (ctx, index) => CategoryItem())),
            ],
          ),
        ),
      ),

    );
  }
}
