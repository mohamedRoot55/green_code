import 'package:flutter/material.dart';
import 'package:greencode_task/constants/colors.dart';
import 'package:greencode_task/constants/image_paths.dart';
import 'package:greencode_task/constants/styles.dart';
import 'package:greencode_task/providers/auth.dart';
import 'package:greencode_task/utils/helper.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  static final String routeName = "/Resgister";

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool value = false;

  final formKey = GlobalKey<FormState>();

  final scaffoldKey = GlobalKey<ScaffoldState>();

  final passwordController = TextEditingController();
  String userName, email, password, gender, phoneNumber;
  FocusNode userNameFocusNode,
      emailFocusNode,
      passwordFocusNode,
      confirmPasswordFocusNode,
      genderFocusNode,
      phoneNumberFocusNode;

  SnackBar snackBar;

  @override
  void initState() {
    super.initState();
    userNameFocusNode = FocusNode();
    emailFocusNode = FocusNode();
    passwordFocusNode = FocusNode();
    confirmPasswordFocusNode = FocusNode();
    genderFocusNode = FocusNode();
    phoneNumberFocusNode = FocusNode();
  }

  validateData() {
    final bool isValidate = formKey.currentState.validate();
    if (!isValidate) {
      return;
    } else if (!value){
      Helper.showSnackBar(snackBar, scaffoldKey, "accept terms and conditions !!") ;
    }else {
      formKey.currentState.save();
      Provider.of<AuthProvider>(context).createUser(
          scaffoldKey: scaffoldKey,
          password: password,
          email: email,
          snackBar: snackBar,
          context: context,
          fullName: userName,
          gender: gender,
          phoneNumber: phoneNumber);
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Consumer<AuthProvider>(
      builder: (ctx, client, ch) {
        return Scaffold(
          key: scaffoldKey,
          body: Form(
            key: formKey,
            child: ModalProgressHUD(
              inAsyncCall: client.loading,
              progressIndicator: CircularProgressIndicator(
                backgroundColor: purple,
              ),
              child: SingleChildScrollView(
                child: Container(
                  height: size.height,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(bgImage), fit: BoxFit.cover)),
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          height: size.height * .1,
                        ),
                        Text(
                          "Register",
                          style: headerStyle,
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          height: size.height * .03,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                focusNode: userNameFocusNode,
                                onSaved: (value) {
                                  userName = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "please enter your full name";
                                  }
                                  return null;
                                },
                                decoration:
                                    getAuthInputDecoration(context, "username"),
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(emailFocusNode);
                                },
                              ),
                            ),
                            SizedBox(
                              height: size.height * .01,
                            ),
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                keyboardType: TextInputType.emailAddress,
                                focusNode: emailFocusNode,
                                onSaved: (value) {
                                  setState(() {
                                    email = value;
                                  });
                                },
                                validator: (value) {
                                  if (value.isEmpty ||
                                      !value.contains("@") ||
                                      !value.contains(".")) {
                                    return "please enter valid email";
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(phoneNumberFocusNode);
                                },
                                decoration:
                                    getAuthInputDecoration(context, "Email"),
                              ),
                            ),
                            SizedBox(
                              height: size.height * .01,
                            ),
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                keyboardType: TextInputType.phone,
                                focusNode: phoneNumberFocusNode,
                                onSaved: (value) {
                                  setState(() {
                                    phoneNumber = value;
                                  });
                                },
                                validator: (value) {
                                  if (value.length < 11) {
                                    return "please enter valid phone number";
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(genderFocusNode);
                                },
                                decoration: getAuthInputDecoration(
                                    context, "phone number"),
                              ),
                            ),
                            SizedBox(
                              height: size.height * .01,
                            ),
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                focusNode: genderFocusNode,
                                onSaved: (value) {
                                  gender = value;
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "please enter your gender";
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(passwordFocusNode);
                                },
                                decoration:
                                    getAuthInputDecoration(context, "gender"),
                              ),
                            ),
                            SizedBox(
                              height: size.height * .01,
                            ),
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                keyboardType: TextInputType.phone,
                                focusNode: passwordFocusNode,
                                controller: passwordController,
                                obscureText: true,
                                onSaved: (value) {
                                  password = Helper.englishNumber(value);
                                },
                                validator: (value) {
                                  if (value.length < 8) {
                                    return "password should be more than 8 numbers or letters";
                                  }
                                  return null;
                                },
                                onFieldSubmitted: (value) {
                                  FocusScope.of(context)
                                      .requestFocus(confirmPasswordFocusNode);
                                },
                                decoration:
                                    getAuthInputDecoration(context, "password"),
                              ),
                            ),
                            SizedBox(
                              height: size.height * .01,
                            ),
                            Container(
                              width: size.width * .7,
                              child: TextFormField(
                                keyboardType: TextInputType.phone,
                                focusNode: confirmPasswordFocusNode,
                                obscureText: true,
                                validator: (value) {
                                  if (Helper.englishNumber(value) !=
                                      passwordController.text) {
                                    return "passwords doesn't matching";
                                  }
                                  return null;
                                },
                                decoration: getAuthInputDecoration(
                                    context, "confirm password"),
                              ),
                            ),
                            SizedBox(
                              height: size.height * .02,
                            ),
                            Container(
                              width: size.width * .7,
                              child: Row(
                                children: <Widget>[
                                  Checkbox(
                                    onChanged: (value) {
                                      setState(() {
                                        this.value = value;
                                      });
                                    },
                                    value: value,
                                  ),
                                  Text("accept conditions and terms")
                                ],
                              ),
                            ),
                            SizedBox(
                              height: size.height * .02,
                            ),
                            GestureDetector(
                              onTap: validateData,
                              child: Container(
                                padding: EdgeInsets.all(10),
                                width: size.width * .7,
                                decoration: BoxDecoration(
                                  color: purple,
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Text(
                                  "sign in",
                                  style: commonWhiteStyle,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: size.height * .05,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
