import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:greencode_task/constants/colors.dart';
import 'package:greencode_task/constants/image_paths.dart';
import 'package:greencode_task/constants/styles.dart';
import 'package:greencode_task/providers/auth.dart';
import 'package:greencode_task/routes/register.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  static final routeName = "/Login";

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  SnackBar snackBar;

  String userEmail;
  String userPassword;

  FocusNode email = new FocusNode();
  FocusNode password = new FocusNode();
  TapGestureRecognizer _gestureRecognizer;

  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  void validateData() async {
    final bool isValidate = formKey.currentState.validate();
    if (!isValidate) {
      return;
    } else {
      formKey.currentState.save();
      Provider.of<AuthProvider>(context).login(
          email: userEmail,
          password: userPassword,

          context: context,
          snackBar: snackBar,
          scaffoldKey: scaffoldKey);
    }
  }

  @override
  void initState() {
    super.initState();
    _gestureRecognizer = TapGestureRecognizer()..onTap = _handlePress;
  }

  void _handlePress() {
    HapticFeedback.vibrate();
    Navigator.of(context).pushNamed(Register.routeName);
  }

  @override
  void dispose() {
    _gestureRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Consumer<AuthProvider>(
      builder: (ctx, client, ch) {
        return   Scaffold(
                key: scaffoldKey,
                body: ModalProgressHUD(
                  inAsyncCall: client.loading,
                  progressIndicator: CircularProgressIndicator(backgroundColor: purple,),
                  child: Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Container(
                        height: size.height,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(bgImage), fit: BoxFit.cover)),
                        child: Center(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: size.height * .1,
                              ),
                              Text(
                                "LOGO",
                                style: logoStyle,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: size.height * .12,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width: size.width * .7,
                                    child: TextFormField(
                                      onSaved: (value) {
                                        setState(() {
                                          userEmail = value;
                                        });
                                      },
                                      validator: (value) {
                                        if (value.isEmpty ||
                                            !value.contains("@") ||
                                            !value.contains(".")) {
                                          return "please enter valid email";
                                        }
                                        return null;
                                      },
                                      focusNode: email,
                                      decoration: getAuthInputDecoration(
                                          context, "email/phone"),
                                      onFieldSubmitted: (value) {
                                        FocusScope.of(context)
                                            .requestFocus(password);
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    height: size.height * .04,
                                  ),
                                  Container(
                                    width: size.width * .7,
                                    child: TextFormField(
                                      obscureText: true,
                                      onSaved: (value) {
                                        setState(() {
                                          userPassword = value;
                                        });
                                      },
                                      validator: (value) {
                                        if (value.length < 8) {
                                          return "password should be more than 8 numbers or letters";
                                        }
                                        return null;
                                      },
                                      focusNode: password,
                                      decoration: getAuthInputDecoration(
                                          context, "password"),
                                    ),
                                  ),
                                  SizedBox(
                                    height: size.height * .01,
                                  ),
                                  Text("forget password ? "),
                                  SizedBox(
                                    height: size.height * .02,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      validateData();
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      width: size.width * .7,
                                      decoration: BoxDecoration(
                                        color: purple,
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      child: Text(
                                        "login",
                                        style: commonWhiteStyle,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: size.height * .05,
                              ),
                              Text(
                                "register with social media account",
                              ),
                              SizedBox(
                                height: size.height * .02,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      Provider.of<AuthProvider>(context)
                                          .signInWithGoogle(
                                          snackBar, scaffoldKey, context);
                                    },
                                    child: Image.asset(
                                      google,
                                      width: 70,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Provider.of<AuthProvider>(context)
                                          .signInWithFacebook(
                                          snackBar, scaffoldKey, context);
                                    },
                                    child: Image.asset(
                                      facebook,
                                      width: 70,
                                    ),
                                  ),
                                  Image.asset(
                                    cloud,
                                    width: 70,
                                  ),
                                ],
                              ),
                              Text.rich(
                                  TextSpan(text: "don't have account", children: [
                                    TextSpan(
                                        text: " register ",
                                        style: TextStyle(
                                            color: purple,
                                            fontWeight: FontWeight.bold),
                                        recognizer: _gestureRecognizer)
                                  ]))
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
      },
    );
  }
}
